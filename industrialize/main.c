#include "config.h"
#include "cenginebase.h"
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#define FPS_LIMIT 60
#define MS_LIMIT 1000 / (float)FPS_LIMIT
float mslimit = MS_LIMIT;

int
main(int argc, char **argv)
{
  generate_ctx();
  cengine_init(640, 480);
  
  fill_color(0xFF, 0xFF, 0xFF);
  char red = 0x00;
  
  bool running = true;
  while(running)
    {
      fpstrack_start();

      fill_color(red, 0xFF, 0xFF);
      red++;

      fpstrack_end();
    }
      
  
  free_ctx();
  return 0;
}
