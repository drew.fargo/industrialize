#include "config.h"
#include "cenginebase.h"

#include <SDL2/SDL.h>

extern CEngine_Context gCTX;

int
fill_color (char red, char green, char blue)
{
  SDL_FillRect (gCTX.surfacep, NULL, SDL_MapRGB (gCTX.surfacep->format, red, green, blue));
  SDL_UpdateWindowSurface (gCTX.windowp);

  return 0;
}
