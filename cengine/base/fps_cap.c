#include "config.h"
#include "cenginebase.h"

#include <SDL2/SDL.h>

#include <stddef.h>

Uint64 gCEngineFpsCtr;
extern float mslimit;

void
fpstrack_start()
{
  gCEngineFpsCtr = SDL_GetPerformanceCounter();
}

void
fpstrack_end()
{
  Uint64 final = SDL_GetPerformanceCounter();
  float msdelta = (final - gCEngineFpsCtr) / (float)SDL_GetPerformanceFrequency() * 1000.0f;
  SDL_Delay(msdelta > mslimit ? 0 : mslimit - msdelta);
}