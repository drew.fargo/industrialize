#ifndef CENGINEBASE_H
#define CENGINEBASE_H

#include <SDL2/SDL.h>
#include <stddef.h>

typedef struct CEngine_Context
{
  SDL_Window *windowp;
  SDL_Surface *surfacep;
} CEngine_Context;

void generate_ctx(void);
void free_ctx();

int fill_color(char red, char green, char blue);
int cengine_init (int width, int height);

void fpstrack_start();
void fpstrack_end();


#endif //CENGINEBASE_H
