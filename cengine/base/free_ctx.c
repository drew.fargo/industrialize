#include "config.h"
#include "cenginebase.h"

#include <stdlib.h>
#include <SDL2/SDL.h>

extern CEngine_Context gCTX;

void
free_ctx(void)
{
  SDL_DestroyWindow(gCTX.windowp);
  SDL_Quit();
}
