#include "config.h"
#include "cenginebase.h"

#include <stdio.h>
#include <SDL2/SDL.h>

extern CEngine_Context gCTX;

int
cengine_init (int width, int height)
{
  if (SDL_Init (SDL_INIT_VIDEO) < 0)
    return -1;

  gCTX.windowp = SDL_CreateWindow ("CEngine Window", SDL_WINDOWPOS_UNDEFINED,
				   SDL_WINDOWPOS_UNDEFINED, width, height,
				   SDL_WINDOW_SHOWN);
  
  if (!(gCTX.windowp))
    return -1;

  gCTX.surfacep = SDL_GetWindowSurface (gCTX.windowp);
  return 0;
}
